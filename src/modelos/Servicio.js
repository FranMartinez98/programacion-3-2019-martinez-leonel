const mongoose = require('mongoose');
const { Schema } = mongoose;

const servicioSchema = new Schema({
  nombre: String,
  tipo: String,
  descripcion: String
  });

mongoose.model('servicios', servicioSchema);