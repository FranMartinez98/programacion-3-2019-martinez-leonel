const mongoose = require('mongoose');
const { Schema } = mongoose;

const clienteSchema = new Schema({
  nombre: String,
  apellido: String,
  dni: Number,
  domicilio: String
  });

mongoose.model('clientes', clienteSchema);