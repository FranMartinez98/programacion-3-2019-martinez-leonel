const mongoose = require('mongoose');
const { Schema } = mongoose;

const productoSchema = new Schema({
  nombre: String,
  marca: String,
  precio: Number,
  descripcion: String,
  cantidad: Number
});

mongoose.model('productos', productoSchema);
