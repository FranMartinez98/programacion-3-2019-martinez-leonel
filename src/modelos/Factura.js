const mongoose = require('mongoose');
const { Schema } = mongoose;

const facturaSchema = new Schema({
  nombre: String,
  marca: String,
  precio: Number,
  descripcion: String
  });

mongoose.model('facturas', facturaSchema);