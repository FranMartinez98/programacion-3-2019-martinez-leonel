const mongoose = require('mongoose');

const Usuario = mongoose.model('login');

module.exports = app => {
    
    app.post('/api/login', async (req, res) => {
        const { usuario, contrasenia } = req.body;
    
        const logeado = new Usuario ({
          usuario,
          contrasenia
        });
        try {
          //objeto de mongose y puede demorar y espera hasta que termine y captura la exc
          let nuevoLogeado = await logeado.save();
          res.status(201).send(nuevoLogeado);
        } catch (err) {
          if (err.name === 'MongoError') {
            res.status(409).send(err.message);
          }
          res.status(500).send(err);
        }
      });

}