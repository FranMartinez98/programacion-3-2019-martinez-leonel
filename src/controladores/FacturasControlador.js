const mongoose = require('mongoose');

const Factura = mongoose.model('facturas');

module.exports = app => {
  app.get('/api/facturas', async (req, res) => {
    console.info('Obteniendo facturas');
    const facturas = await Factura.find({});
    res.send(facturas);
  });


  app.get('/api/facturas/:id', async (req, res) => {
    try {
      const id = req.params.id;
      const factura = await Factura.findById(id);

      if (factura) {
        res.send(factura);
      } else {
        res
          .status(404)
          .send({ mensaje: `La factura con id '${id}' no ha sido encontrada.` });
      }
    } catch (e) {
      res.status(500).send({ mensaje: `Error en el servidor.\n\n${e}` });
    }
  });

  app.post('/api/facturas', async (req, res) => {
    const { nombre, marca, precio, descripcion  } = req.body;

    const factura = new Factura({
      nombre,
      marca,
      precio,
      descripcion  
    });
    try {
      //objeto de mongose y puede demorar y espera hasta que termine y captura la exc
      let nuevoFactura = await factura.save();
      res.status(201).send(nuevoFactura);
    } catch (err) {
      if (err.name === 'MongoError') {
        res.status(409).send(err.message);
      }
      res.status(500).send(err);
    }
  });

  app.put('/api/facturas/:id', async (req, res) => {
    const id = req.params.id;
    const datosFactura = req.body || {}; //objeto vacío 
    delete datosFactura.findByIdAndUpdate;
  

    try {
      let factura = await Factura.findByIdAndUpdate({ _id: id }, datosFactura, {
        new: true
      });

      if (!factura) {
        res.status(404).send({
          mensaje: `Error cuando se actualizaba el prod con id ${id}.\n\n${e}`
        });
      } else {
        res.status(200).send(factura);
      }
    } catch (err) {
      if (err.name === 'MongoError') {
        res.status(409).send({ mensaje: err.message });
      }
      
      res.status(500).send({
        mensaje: `Error desconocido.\n\n Error desconocido cuando se actualizaba el prod id='${id}'`
      });
    }
  });

  app.delete('/api/facturas/:id', async (req, res) => {
    const id = req.params.id;

    try {
      let factura = await Factura.findByIdAndRemove({ _id: id });

      if (!factura) {
        return res.status(404).send({ mensaje: 'Factura no encontrada' });
      } else {
        return res.status(204).send({ message: 'Factura Eliminado' }); // 204 do not use content
      }
    } catch (err) {
      return res.status(500).send({
        mensaje: `Error desconocido cuando se borraba factura con id '${id}'.`
      });
    }
  });

};