import axios from 'axios';
import { tipoLogeado } from './types';

export const logear = (usuario,password) => async dispatch => {
  dispatch({ type: tipoLogeado.LOGIN_PENDIENTE });

  try {
    var res = await axios.post('/api/login',{usuario,password});
    dispatch({ type: tipoLogeado.LOGIN_TERMINADO, payload: res });
  } catch (error) {
    dispatch({ type: tipoLogeado.LOGIN_RECHAZADO, payload: error });
  }
};