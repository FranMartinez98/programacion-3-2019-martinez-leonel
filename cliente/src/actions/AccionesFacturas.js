import axios from 'axios';
import { tiposFactura } from './types';

export const buscarFacturas = () => async dispatch => {
  dispatch({ type: tiposFactura.BUSCAR_PRODUCTOS_PENDIENTE});

  try {
    var res = await axios.get('/api/facturas');
    dispatch({ type: tiposFactura.BUSCAR_FACTURAS_TERMINADA, payload: res });
  } catch (error) {
    dispatch({ type: tiposFactura.BUSCAR_FACTURAS_RECHAZADA, payload: error });
  }
};


export const guardarFactura = factura => async dispatch => {
  var res = await axios.post('/api/facturas', factura);
  dispatch({ type: tiposFactura.GUARDAR_FACTURA, payload: res });
};

export const buscarFacturasPorId = id => async dispatch => {
  dispatch({ type: tiposFactura.BUSCAR_FACTURAS_POR_ID_PENDIENTE });

  try {
    var res = await axios.get('/api/facturas/' + id);
    dispatch({
      type: tiposFactura.BUSCAR_FACTURAS_POR_ID_TERMINADA, payload: res
    });
  } catch (error) {
    dispatch({ type: tiposFactura.BUSCAR_FACTURAS_POR_ID_RECHAZADA });
  }
};

export const actualizarFactura = factura => async dispatch => {
  const res = await axios.put(`/api/facturas/${factura._id}`, factura);
  return dispatch({
    type: tiposFactura.ACTUALIZAR_FACTURA,
    payload: res
  });
};

export const eliminarFactura = id => async dispatch => {
  let res = await axios.delete(`/api/facturas/${id}`);

  if (res.status === 204) {
    res._id = id;
  }

  return dispatch({
    type: tiposFactura.ELIMINAR_FACTURA,
    payload: res
  });
};