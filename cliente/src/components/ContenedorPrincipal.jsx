import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Bienvenidos from './bienvenidosPaginas/Bienvenidos';
import ListarTarea from './tareasPaginas/ListarTarea';
import NuevaTarea from './tareasPaginas/NuevaTarea';
import VerTarea from './tareasPaginas/VerTarea';
import EditarTarea from './tareasPaginas/EditarTarea';
import ListarProducto from './productosPaginas/ListarProducto';
import NuevoProducto from './productosPaginas/NuevoProducto';
import VerProducto  from './productosPaginas/VerProducto';
import EditarProducto from './productosPaginas/EditarProducto';
import ListarServicio from './serviciosPaginas/ListarServicio';
import NuevoServicio from './serviciosPaginas/NuevoServicio';
import VerServicio  from './serviciosPaginas/VerServicio';
import EditarServicio from './serviciosPaginas/EditarServicio';
import ListarCliente from './clientesPaginas/ListarCliente';
import NuevoCliente from './clientesPaginas/NuevoCliente';
import VerCliente  from './clientesPaginas/VerCliente';
import EditarCliente from './clientesPaginas/EditarCliente';
//import Factura from './facturaPaginas/Factura';


class ContenedorPrincipal extends Component {
  render() {
    return (
      <div>
        <div className='container-fluid'>
          <div className='row' >
            <div className='col-md-9 ml-sm-auto col-lg-10 px-4' >
              <Route exact path='/bienvenidos' component={Bienvenidos}/>
              <Route exact path='/tareas' component={ListarTarea} />
              <Route exact path='/tareas/nueva' component={NuevaTarea} />
              <Route exact path='/tareas/:id/ver' component={VerTarea} />
              <Route exact path='/tareas/:id/editar' component={EditarTarea} />
              <Route exact path='/productos' component={ListarProducto} />
              <Route exact path='/productos/nuevo' component={NuevoProducto} />
              <Route exact path='/productos/:id/ver' component={VerProducto} />
              <Route exact path='/productos/:id/editar' component={EditarProducto}/>
              <Route exact path='/servicios' component={ListarServicio} />
              <Route exact path='/servicios/nuevo' component={NuevoServicio} />
              <Route exact path='/servicios/:id/ver' component={VerServicio} />
              <Route exact path='/servicios/:id/editar' component={EditarServicio}/>
              <Route exact path='/clientes' component={ListarCliente} />
              <Route exact path='/clientes/nuevo' component={NuevoCliente} />
              <Route exact path='/clientes/:id/ver' component={VerCliente} />
              <Route exact path='/clientes/:id/editar' component={EditarCliente}/>
              
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContenedorPrincipal;
