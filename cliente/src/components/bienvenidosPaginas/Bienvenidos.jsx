import React, { Component } from 'react';
import musica from './images-bienvenidos/musica.png';
import placa from './images-bienvenidos/placa.png';
import teclado from './images-bienvenidos/teclado.png';
import webcam from './images-bienvenidos/webcam.png';
import pc from './images-bienvenidos/pc.png';
import mouse from './images-bienvenidos/mouse.png';
import camara from './images-bienvenidos/camara.png';
import notebook from './images-bienvenidos/notebook.png';
import netbook from './images-bienvenidos/netbook.png';
import televisor from './images-bienvenidos/televisor.png';
import micro from './images-bienvenidos/micro.png';
import audifonos from './images-bienvenidos/audifonos.png';
import xbox from './images-bienvenidos/xbox.png';
import play4 from './images-bienvenidos/play4.png';
import monitor from './images-bienvenidos/monitor.png';
import fuente from './images-bienvenidos/fuente.png';


class Bienvenidos extends Component {
  render() {
    return (
      <div >
        <p className='bienvenidos' >
          Technology Market
        </p>
        <br/>
        <table className='table'>
          <tbody>
          <tr>
              <td><p>Computadoras</p>
              <img src={pc} className='items-images' alt="pc"/>
              </td>
              <td><p>Mouse inalámbricos</p>
              <img src={mouse} className='items-images' alt="mouse"/>
              </td>
              <td><p>WebCam</p>
                <img src={webcam} className='items-images' alt="webcam"/>
              </td>
              <td><p>Tarjetas de Video Radeon</p>
                <img src={placa} className='items-images' alt="placa"/>
              </td>
              <td><p>Teclados</p>
                <img src={teclado} className='items-images' alt="teclado"/>
              </td>
              <td><p>Equipos de música</p>
                <img src={musica} className='items-images' alt="musica"/>
              </td>
              <td><p>Fuentes de alimentación</p>
              <img src={fuente} className='items-images' alt="fuente"/>
              </td>
              <td><p>Cámaras Profesionales</p>
              <img src={camara} className='items-images' alt="camara"/>
              </td>
          </tr>
          <tr>
            <td><p>Monitores de PC</p>
            <img src={monitor} className='items-images' alt="monitor"/>
            </td>
            <td><p>Televisores</p>
              <img src={televisor} className='items-images' alt="televisor"/>
            </td>
            <td><p>Play Station 4</p>
            <img src={play4} className='items-images' alt="play4"/>
            </td>
            <td><p>Notebook</p>
            <img src={notebook} className='items-images' alt="notebook"/>
            </td>
            <td><p>Netbook</p>
            <img src={netbook} className='items-images' alt="netbook"/>
            </td>
            <td><p>Xbox 360</p>
            <img src={xbox} className='items-images' alt="xbox"/>
            </td>
            <td><p>Auriculares SoundRound</p>
            <img src={audifonos} className='items-images' alt="audifonos"/>
            </td>
            <td><p>Micrófonos PC</p>
            <img src={micro} className='items-images' alt="micro"/>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Bienvenidos;
