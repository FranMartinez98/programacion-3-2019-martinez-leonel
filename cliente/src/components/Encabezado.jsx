import React, { Component } from 'react';
import logo192 from './images/logo192.png';
import {Route,Link} from 'react-router-dom';
import InicioSesion from './InicioSesion';
import Contenedor from './Contenedor';
import cierre from './images/cierre.png';

class Encabezado extends Component {
  render() {
    return (
      <div className='contenidoEncabezado'>
        <div className='encabezado'>
          <div className='navbar-brand tituloEncabezado'>
            <img src={logo192} className='logo' alt='logo192' /> 
              TECHNOLOGY MARKET
          </div>
          <nav className='navEncabezado'>
            <Link to='/'>
            <button className=' navbar-brand link-encabezado btn btn-outline-light' >
            Salir
            <img src={cierre} className='icono-cierre' alt='cierre'/>
            </button>
            </Link>
          </nav>
        </div>
        <Route exact path='/' component={InicioSesion}/>
        <Route exact path='/contenedor' component={Contenedor}/>
      </div>
    );
  }
}

export default Encabezado;
