import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarClientePorId } from '../../actions/AccionesClientes';
import { Link } from 'react-router-dom';

class VerCliente extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.id = id;
    if (id) {
      this.props.buscarClientePorId(id);
    }
  }

  render() {
    return (
      <div>
        <h2 className='mr-2 margin-tittle-cliente'>Ver Cliente</h2>

        <br />
        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Nombre:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.cliente.nombre}</div>
        </div>
        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Apellido:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.cliente.apellido}</div>
        </div>
        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>DNI:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.cliente.dni}</div>
        </div>
        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Domicilio:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.cliente.domicilio}</div>
        </div>
        <div className='row'>
          <Link className='btn btn-danger mr-2 margin-button-cliente' to='/clientes'>
            Volver
          </Link>
          <Link
            to={`/clientes/${this.id}/editar`}
            className='btn btn-info mr-2'
          >
            Editar
           </Link>
          &nbsp;
        </div>
      </div>
    );
  }
}

function mapState(state) {
  return {
    cliente: state.clientesDs.cliente
  };
}

const actions = {
  buscarClientePorId
};

export default connect(
  mapState,
  actions
)(VerCliente);
