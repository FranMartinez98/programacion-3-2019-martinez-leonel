import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarClientes, eliminarClientes } from '../../actions';
import { Link } from 'react-router-dom';

class ListarCliente extends Component {
  componentWillMount() {
    this.props.buscarClientes();
  }

  crearFilas() {
    return this.props.listaClientes.map(cliente => {
      return (
        <tr key={cliente._id}>
          <td>{cliente.nombre}</td>
          <td>{cliente.apellido}</td>
          <td>{cliente.dni}</td>
          <td>{cliente.domicilio}</td>
          <td>
            <Link to={`/clientes/${cliente._id}/ver`} className='mr-2 btn btn-outline-success'>
              Ver
            </Link>
            <Link to={`/clientes/${cliente._id}/editar`} className='mr-2 btn btn-outline-warning'>
              Editar
            </Link>
            <Link
              className='mr-2 btn btn-outline-danger'
              to='/clientes'
              onClick={() => {
                if (
                  window.confirm(
                    '¿Está usted seguro que desea eliminar el cliente?'
                  )
                )
                  this.props.eliminarClientes(cliente._id);

              }}
            >
              Eliminar
            </Link>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div>
        <h2 className='mr-2'>Listando Clientes</h2>

        <p>
          <Link to='/clientes/nuevo' className='btn btn-primary'>
            Nuevo
          </Link>
        </p>

        <div className='table-responsive'>
          <table className='table table-hover table-dark table-sm'>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>DNI</th>
                <th>Domicilio</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>{this.crearFilas()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapState(state) {
  return {
    listaClientes: state.clientesDs.listaClientes
  };
}

const actions = {
  buscarClientes,
  eliminarClientes
};

export default connect(
  mapState,
  actions
)(ListarCliente);
