import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import home from './images/home.png';
import tareas from './images/tareas.png';
import productos from './images/productos.png';
import servicios from './images/servicios.png';
import clientes from './images/clientes.png';
//import facturas from './images/facturas.png';

class MenuIzquierdo extends Component {
  render() {
    return (
      <nav className='col-md-2 d-none d-md-block bg-light sidebar'>
        <div className='sidebar-sticky'>
          <ul className='nav flex-column'>
            <li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/bienvenidos'}>
                <img src={home} className='iconos' alt='home'/>
                Inicio
              </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/tareas'}>
              <img src={tareas} className='iconos' alt='tareas'/>
                Tareas
              </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/productos'} >
                <img src={productos} className='iconos' alt='productos'/>
                  Productos
              </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/servicios'}>
              <img src={servicios} className='iconos' alt='servicios'/>
                Servicios
              </NavLink>
            </li>

            <li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/clientes'}>
              <img src={clientes} className='iconos' alt='clientes'/>
                Clientes
              </NavLink>
            </li>

            

          </ul>
        </div>
      </nav>
    );
  }
}

export default MenuIzquierdo;
/*<NavLink exact={true} className='nav-link imagen' to={'/productos'}>
              <img src={productos} className='iconos' alt='productos'/>
                Productos
              </NavLink>*/

/*
<li className='nav-item'>
              <NavLink exact={true} className='nav-link imagen' to={'/facturas'}>
              <img src={facturas} className='iconos' alt='facturas'/>
                Facturación
              </NavLink>
            </li>

*/