import React, { Component } from 'react';
import './dashboard.css';
import oficina from './images/oficina.jpg';
import { SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { logear } from './../actions/AccionesLogeado';
//import { Redirect } from 'react-router';

class InicioSesion extends Component {

  validar = e =>{
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;

    return this.props
      .logear(username,password)
      .then(response => window.location='/contenedor')
      .catch(err => {
        throw new SubmissionError(this.props.errores);
      });

  }
       
    /*if (username === 'admin' && password === 'cod'){
      alert('Login successsfully');
      window.location= '/contenedor';
      reset();
      return false;
    }else{
      alert('Access denied');
      window.location= '/';
      reset();
      return false;
    }
  function reset() {
    document.getElementById('username').reset();
    document.getElementById('password').reset();
  }*/
    
  render() {
    return (
      <div >
        <div className="card bg-dark text-white">
          <img src={oficina} className='bodyInicioSesion' alt="oficina"/>
            <div className="card-img-overlay ">
              <div className="contLogin">
                <div className="inputLogin">
                  <h2>Inicio de Sesión</h2><br/>
                  <input type='text' placeholder='Usuario' id='username' required/>
                  <input type='password' placeholder='Contraseña' id='password' required/>
                  <input type='button' value='Ingresar' onClick={this.validar} />  
                </div> 
              </div>
            </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.loginDs.login,
    errores: state.loginDs.errores
  };
}

export default connect (
  mapStateToProps,
  {logear}
)(InicioSesion);


//figure-img img-fluid