import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarServicios, eliminarServicios } from '../../actions';
import { Link } from 'react-router-dom';

class ListarServicio extends Component {
  componentWillMount() {
    this.props.buscarServicios();
  }

  crearFilas() {
    return this.props.listaServicios.map(servicio => {
      return (
        <tr key={servicio._id}>
          <td>{servicio.nombre}</td>
          <td>{servicio.tipo}</td>
          <td>{servicio.descripcion}</td>
          <td>
            <Link to={`/servicios/${servicio._id}/ver`} className='mr-2 btn btn-outline-success'>
              Ver
            </Link>
            <Link to={`/servicios/${servicio._id}/editar`} className='mr-2 btn btn-outline-warning'>
              Editar
            </Link>
            <Link
              className='mr-2  btn btn-outline-danger'
              href='/servicios'
              onClick={() => {
                if (
                  window.confirm(
                    '¿Está usted seguro que desea eliminar el servicio?'
                  )
                )
                  this.props.eliminarServicios(servicio._id);

              }}
            >
              Eliminar
            </Link>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div>
        <h2 className='mr-2'>Listando Servicios</h2>

        <p>
          <Link to='/servicios/nuevo' className='btn btn-primary'>
            Nuevo
          </Link>
        </p>

        <div className='table-responsive'>
          <table className='table table-hover table-dark table-sm'>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Descripcion</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>{this.crearFilas()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapState(state) {
  return {
    listaServicios: state.serviciosDs.listaServicios
  };
}

const actions = {
  buscarServicios,
  eliminarServicios
};

export default connect(
  mapState,
  actions
)(ListarServicio);