import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarServicioPorId } from '../../actions/AccionesServicios';
import { Link } from 'react-router-dom';

class VerServicio extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.id = id;
    if (id) {
      this.props.buscarServicioPorId(id);
    }
  }

  render() {
    return (
      <div>
        <h2 className='mr-2 margin-tittle-servicio'>Ver Servicio</h2>

        <br />

        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Nombre:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.servicio.nombre}</div>
        </div>

        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Tipo:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.servicio.tipo}</div>
        </div>

        <div className='row'>
          <div className='col-sm-2'>
            <p className='font-weight-bold text-right mr-2'>Descripcion:</p>
          </div>
          <div className='col-sm-10 tbody'>{this.props.servicio.descripcion}</div>
        </div>

        <div className='row'>
          <Link className='btn btn-danger mr-2 margin-button-servicio' to='/servicios'>
            Volver
          </Link>
          <Link
            to={`/servicios/${this.id}/editar`}
            className='btn btn-info mr-2'
          >
            Editar
          </Link>
          &nbsp;
        </div>
      </div>
    );
  }
}

function mapState(state) {
  return {
    servicio: state.serviciosDs.servicio
  };
}

const actions = {
  buscarServicioPorId
};

export default connect(
  mapState,
  actions
)(VerServicio);