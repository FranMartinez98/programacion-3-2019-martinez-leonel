import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './dashboard.css';
import Encabezado from './Encabezado';



class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Encabezado/>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
