import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarProductos, eliminarProductos, guardarFactura } from '../../actions';
import { Link } from 'react-router-dom';
import carrito from '../images/carrito.png';


class ListarProducto extends Component {
  
  componentWillMount() {
    this.props.buscarProductos();
  }
  
  constructor(props) {
    super(props);

    this.state = {
      lleno: false,
      nombre: "",
      marca: "",
      descripcion: "",
      precio: "",

      cantidad: 0,
      precioTotal: 0
    }
  }
  
  onChange(nombre,marca,descripcion,precio) {
    this.setState({ nombre: nombre })
    this.setState({ marca: marca })
    this.setState({ descripcion: descripcion })
    this.setState({ precio: precio })
    this.setState({ lleno: true })
  }

  incrementar() {
    this.setState({ cantidad: this.state.cantidad + 1 })
  }

  reducir() {
    if(this.state.cantidad !== 0){
      this.setState({ cantidad: this.state.cantidad - 1 })
    }
  }

  calculoPrecioTotal() {
    let precioMultiplicar = this.state.precio;
    let cantidadMultiplicar = this.state.cantidad;
    return(
      precioMultiplicar * cantidadMultiplicar
    );
  }

  crearTabla() {
    if(this.state.lleno === true){
      return(
          <tbody>
            <tr>
              <td >
                {this.state.nombre} <br/>
                {this.state.marca} <br/>
                {this.state.descripcion} <br/>
              </td>
              <td>$ {this.state.precio}</td>
              <td> 
                <div className="col" >
                  {this.state.cantidad}
                  <button className="inputAgregarCantidad btn btn-info" onClick={this.incrementar.bind(this)} >+</button> 
                  <button className="inputRestarCantidad btn btn-info" onClick={this.reducir.bind(this)} >-</button> 
                </div>
              </td>
              <td className="colorPrecioTotal" >$ {this.calculoPrecioTotal()}</td>
    
            </tr>
          </tbody>
      );
    }else{
      console.log("No se pudo agregar producto");
    }
  }


  crearFilas() {
    return this.props.listaProductos.map(producto => {
      return (
        <tr key={producto._id}>
          <td>{producto.nombre}</td>
          <td>{producto.marca}</td>
          <td>$ {producto.precio}</td>
          <td>{producto.descripcion}</td>
          <td>{producto.cantidad}</td>
          <td className='posicionItemTabla' >
            <Link to={`/productos/${producto._id}/ver`} className='mr-2 btn btn-outline-success'>
              Ver
            </Link>
            <Link to={`/productos/${producto._id}/editar`} className='mr-2 btn btn-outline-warning'>
              Editar
            </Link>
            <Link
              className='mr-2 btn btn-outline-danger'
              to='/productos'
              onClick={() => {
                if (
                  window.confirm(
                    '¿Está usted seguro que desea eliminar el producto?'
                  )
                )
                  this.props.eliminarProductos(producto._id);

              }}
            >
              Eliminar
            </Link>
            <button className='mr-2 btn btn-outline-info' 
            onClick={() => {
              this.onChange(producto.nombre,producto.marca,producto.descripcion,producto.precio);
            }} >
                <img src={carrito} className='icono-carrito' alt="carrito"/>
            </button>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div>
        <h2 className='mr-2'>Listando Productos</h2>

        <p>
          <Link to='/productos/nuevo' className='btn btn-primary'>
            Nuevo
          </Link>
        </p>

        <div className='table-responsive'>
          <table className='table table-hover table-dark table-sm'>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Precio</th>
                <th>Descripcion</th>
                <th>Cantidad</th>
                <th className='posicionItemTabla' >Acciones</th>
              </tr>
            </thead>
            <tbody>{this.crearFilas()}</tbody>
          </table>
        </div>
        
        <h2 className='mr-2'>Facturación</h2>
        <div className='table-responsive'>
          <table className='table table-hover table-dark table-sm'>
              <thead>
                <tr>
                  <th>Producto</th>
                  <th>Precio unitario</th>
                  <th>Cantidad</th>
                  <th>Precio total</th>
                  <th></th>
                </tr>
              </thead>
              {this.crearTabla()}
          </table>
        </div>
        
      </div>    
    );
  }
}


function mapState(state) {
  return {
    listaProductos: state.productosDs.listaProductos,
    listaFactura: state.facturasDs.listaFactura
  };
}

const actions = {
  buscarProductos,
  eliminarProductos,
  guardarFactura
};

export default connect(
  mapState,
  actions
)(ListarProducto);