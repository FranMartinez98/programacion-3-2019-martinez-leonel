import React, {Component} from 'react';
import { connect } from 'react-redux';
import { buscarProductos, buscarClientes, buscarServicios ,eliminarProductos } from '../../actions';
//import { Link } from 'react-router-dom';

import 'moment/locale/it.js';
import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import cliente from './images-factura/cliente.png';
import servicio from './images-factura/servicio.png';
import insertar from './images-factura/insertar.png';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';


class Factura extends Component {

    constructor(props) {
        super(props);

        // Initial state with date
        this.state = {
            // or Date or Moment.js
            selectedDate: "",
            valueProducto: 'Seleccione producto',
            valueCliente: 'Seleccione cliente',
            valueServicio: 'Seleccione servicio',
        };

        // This binding is necessary to make `this` work in the callback
        this.onChange = this.onChange.bind(this);
    }


    onChange(e) {
        this.setState({ selectedDate: e });
        console.log(e);
    }

    handleChangeP(e) {
        this.setState({ valueProducto: e });
        console.log(e);
    }


    handleChangeC(e) {
        this.setState({ valueCliente: e })
        console.log(e)
    }
    
    handleChangeS(e) {
        this.setState({ valueServicio: e })
        console.log(e)
    }

    componentWillMount() {
        this.props.buscarProductos();
        this.props.buscarClientes();
        this.props.buscarServicios();
    }

    mostrarProductos() {
        return this.props.listaProductos.map(producto => {
            return(
                <option>{producto.nombre} {producto.marca} {producto.descripcion}</option> 
            );
        })
    }

    mostrarClientes() {
        return this.props.listaClientes.map(cliente => {
            return(
                <option>{cliente.nombre} {cliente.apellido} {cliente.dni}</option> 
            );
        })
    }

    mostrarServicios() {
        return this.props.listaServicios.map(servicio => {
            return(
                <option>{servicio.nombre}-{servicio.descripcion}</option> 
            );
        })
    }

   
    render(){
        return(
            <div>
                <h2 className='mr-2'>Facturación</h2>
            <br/>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div > 
                            <DatePickerInput className='datepicker btn btn-primary'
                                onChange={this.onChange}
                                value={this.state.selectedDate}
                            />
                        </div>
                    </div>
                    <div className="col">
                        <div>
                            <img src={cliente} className='icono-cliente-factura' alt="cliente"/>
                                <Dropdown className='desplegable-cliente btn btn-primary'
                                    value={this.state.valueCliente}
                                    onChange={this.handleChangeC.bind(this)}
                                    options={this.mostrarClientes()}
                                />
                        </div>
                    </div>
                </div>
                <br/>
                <div className='row'>
                        <div className="col">
                            <Dropdown className='desplegable-factura btn btn-primary'
                                value={this.state.valueProducto}
                                onChange={this.handleChangeP.bind(this)}
                                options={this.mostrarProductos()}
                            />
                        </div>
                        <div className="col">
                            <img src={servicio} className='icono-cliente-factura' alt="servicio"/>
                            <Dropdown className='desplegable-servicio btn btn-primary'
                                value={this.state.valueServicio}
                                onChange={this.handleChangeS.bind(this)}
                                options={this.mostrarServicios()}
                            />
                            <button className='btn btn-outline-light'>
                                <img src={insertar} className='icono-insertar' alt="insertar"/>
                            </button>
                        </div>
                    </div>
                </div>
                <br/>
            <div className="container" >
                <div className="row" >
                    <div className="col" >
                        <p className="mr-2">Producto</p>

                    </div>
                </div>
            </div>

            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
      listaProductos: state.productosDs.listaProductos,
      listaClientes: state.clientesDs.listaClientes,
      listaServicios: state.serviciosDs.listaServicios,
      
    };
}
  
const actions = {
    buscarProductos,
    buscarClientes,
    buscarServicios,
    eliminarProductos
};

export default connect(
    mapStateToProps,
    actions
)(Factura);


/*<div className='table-responsive'>
                    <table className='table table-hover table-dark table-sm'>
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{this.handleChangeP}</td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>*/

    /*crearFilas() {
        <input type="text"  placeholder='Cliente' id='cliente-factura' required/>
        return this.props.listaProductos.map(producto => {
          return (  
            <tr key={producto._id}>
              <td>{producto.nombre}</td>
              <td>{producto.marca}</td>
              <td>{producto.precio}</td>
              <td>{producto.descripcion}</td>
              <td></td>
              <td>
                <Link
                  className='mr-2 btn btn-outline-danger'
                  to='/productos'
                  onClick={() => {
                    if (
                      window.confirm(
                        '¿Está usted seguro que desea eliminar el producto?'
                      )
                    )
                      this.props.eliminarProductos(producto._id);
    
                  }}
                >
                  Eliminar
                </Link>
              </td>
            </tr>
          );
        });
        <div className='table-responsive'>
                    <table className='table table-hover table-dark table-sm'>
                        <thead>
                            <tr>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Detalle</th>
                            <th>Precio Unitario</th>
                            <th>Cantidad</th>
                            <th></th>
                            </tr>
                        </thead>
                            <tbody><TablaFactura/></tbody>
                    </table>
                </div>
      }*/

      /*
      producto: state.productosDs.producto
      */