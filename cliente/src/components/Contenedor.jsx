import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './dashboard.css';
import MenuIzquierdo from './MenuIzquierdo';
import ContenedorPrincipal from './ContenedorPrincipal';

class Contenedor extends Component {
    render() {
        return(
            <div>
                <BrowserRouter>
                 <div>
                    <ContenedorPrincipal/>
                    <MenuIzquierdo/>
                 </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default Contenedor;