import { tipoLogeado } from '../actions/types';

const ESTADO_INICIAL = {
  logeado: false,
  cargando: false,
  errores: {}
};

export default function(state = ESTADO_INICIAL, action) {
  switch (action.type) {
    case tipoLogeado.LOGIN_PENDIENTE:
      return {
        ...state,
        cargando: true
      };

    case tipoLogeado.LOGIN_TERMINADO:
      return {
        ...state,
        cargando: false,
        logeado: true
      };
    
    case tipoLogeado.LOGIN_RECHAZADO:
        return {
        ...state,
        cargando: false,
        logeado: false
    };


    default:
      return state;
  }
}