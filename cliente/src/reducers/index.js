import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import reducerTareas from './ReductorTareas';
import reducerProductos from './ReductorProductos';
import reducerServicios from './ReductorServicios';
import reducerClientes from './ReductorClientes';
import reducerFacturas from './ReductorFactura';
import reducerLogin from './ReductorLogin';

export default combineReducers({
  form: reduxForm,
  tareasDs: reducerTareas,
  productosDs: reducerProductos,
  serviciosDs: reducerServicios,
  clientesDs: reducerClientes,
  facturasDs: reducerFacturas,
  loginDs: reducerLogin
});
