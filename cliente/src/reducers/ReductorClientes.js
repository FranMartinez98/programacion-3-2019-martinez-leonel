import { tiposClientes } from '../actions/types';

const ESTADO_INICIAL = {
  listaClientes: [],
  cliente: {},
  cargando: false,
  errores: {}
};

export default function(state = ESTADO_INICIAL, action) {
  switch (action.type) {
    case tiposClientes.BUSCAR_CLIENTES_TERMINADA:
      return {
        ...state,
        listaClientes: action.payload.data,
        cargando: false
      };

    case tiposClientes.BUSCAR_CLIENTES_PENDIENTE:
      return {
        ...state,
        cargando: true
      };

    case tiposClientes.BUSCAR_CLIENTES_RECHAZADA:
      return {
        ...state,
        cargando: false
      };

    case tiposClientes.BUSCAR_CLIENTES_POR_ID_TERMINADA: {
      return {
        ...state,
        cliente: action.payload.data,
        cargando: false
      };
    }

    case tiposClientes.BUSCAR_CLIENTES_POR_ID_PENDIENTE: {
      return {
        ...state,
        cliente: {},
        cargando: true
      };
    }

    case tiposClientes.BUSCAR_CLIENTES_POR_ID_RECHAZADA: {
      return {
        ...state,
        cargando: false
      };
    }

    case tiposClientes.ELIMINAR_CLIENTE: {
      const id = action.payload._id;
      return {
        ...state,
        listaClientes: state.listaClientes.filter(item => item._id !== id)
      };
    }

    default:
      return state;
  }
}