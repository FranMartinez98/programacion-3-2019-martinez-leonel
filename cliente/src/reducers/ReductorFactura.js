import { tiposFactura } from '../actions/types';

const ESTADO_INICIAL = {
  listaFactura: [],
  factura: {},
  cargando: false,
  errores: {}
};

export default function (state = ESTADO_INICIAL, action) {
  switch (action.type) {
    case tiposFactura.BUSCAR_FACTURAS_TERMINADA:
      return {
        ...state,
        listaFactura: action.payload.data,
        cargando: false
      };

    case tiposFactura.BUSCAR_FACTURAS_PENDIENTE:
      return {
        ...state,
        cargando: true
      };


    case tiposFactura.BUSCAR_FACTURAS_RECHAZADA:
      return {
        ...state,
        cargando: false
      };

    case tiposFactura.BUSCAR_FACTURAS_POR_ID_TERMINADA: {
      return {
        ...state,
        factura: action.payload.data,
        cargando: false
      };
    }

    case tiposFactura.BUSCAR_FACTURAS_POR_ID_PENDIENTE: {
      return {
        ...state,
        factura: {},
        cargando: true
      };
    }

    case tiposFactura.BUSCAR_FACTURAS_POR_ID_RECHAZADA: {
      return {
        ...state,
        cargando: false
      };
    }

    case tiposFactura.ELIMINAR_FACTURA: {
      const id = action.payload._id;
      return {
        ...state,
        
        listaFactura: state.listaFactura.filter(item => item._id !== id)

      };
    }

    default:
      return state;
  }
}